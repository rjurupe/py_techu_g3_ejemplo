var express = require('express');
var router = express.Router();
var middleware = require('../util/middleware')
require('dotenv').config();

var user_controller = require('../controllers/user_controller');

router.get('/',middleware.ensureAuthenticated, user_controller.getAll);
router.post('/', user_controller.userCreate);
router.get('/:id',middleware.ensureAuthenticated, user_controller.userById);
router.put('/:id',middleware.ensureAuthenticated, user_controller.userUpdate);
router.delete('/:id',middleware.ensureAuthenticated, user_controller.userDelete);
router.post('/login2', user_controller.userLogin2);
module.exports = router;
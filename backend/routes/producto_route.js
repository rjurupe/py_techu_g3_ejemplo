var express = require('express');
var router = express.Router();
var middleware = require('../util/middleware')
require('dotenv').config();

var producto_controller = require('../controllers/producto_controller');

router.get('/',middleware.ensureAuthenticated, producto_controller.getAll);
router.post('/',middleware.ensureAuthenticated, producto_controller.productoCreate);
router.put('/:id',middleware.ensureAuthenticated, producto_controller.productoUpdate);
/*
router.get('/:id',middleware.ensureAuthenticated, producto_controller.productoById);
router.delete('/:id',middleware.ensureAuthenticated, producto_controller.productoDelete);
*/
module.exports = router;
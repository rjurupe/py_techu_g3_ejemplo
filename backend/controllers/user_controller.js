var User = require('../models/user_model');
var util_sisgein = require('../util/util_sisinv');
var jwt = require('jsonwebtoken');
var bcrypt = require('bcrypt');
const SALT_ROUNDS = process.env.SALT_ROUNDS;
const JWT_SECRET_PASS = process.env.JWT_SECRET_PASS;
require('dotenv').config();

//Simple version, without validation or sanitation
exports.getAll = function (req, res) {
    try{
        User.find({}).exec(function (err, User) {
            if (err) {
                res.status(400);
                res.send({"mensaje":"Ocurrio un error al obtener los usuarios ","detalle":err.message});
            }else{
                res.status(200);
                res.send({"Usuarios":User});
            }            
        });
    }catch(ex){
        res.status(500);
        res.send({"detalle":ex+""});
    }
};

exports.userCreate = function (req, res) {
    var password = req.body.password;  
    var hashPassword = bcrypt.hashSync(password, parseInt(SALT_ROUNDS));  
    try{

        User.findOne({email:req.body.email}, function (err, fUser) {       
            if (err) {
                res.status(400);
                res.send({"mensaje":"Ocurrió un error al registrar el usuario","detalle":err.message});
            }else{
                if(fUser!=null){
                    res.status(400);
                    res.send({"mensaje":"El usuario ya se encuentra registrado.","user":fUser});
                }else{
                    var  user = new User(
                        {
                            first_name: req.body.first_name,
                            last_name: req.body.last_name,
                            email : req.body.email,
                            password : hashPassword,
                            rol : req.body.rol,
                            logged : req.body.logged
                        }
                    );
                    user.save(function (err) {            
                            if (err) {
                                res.status(400);
                                res.send({"mensaje":"Ocurrio un error al registrar el usuario ","detalle":err.message});
                            }else{
                                res.status(200);
                                if(user!=null){
                                    res.send({"mensaje":"Usuario agregado satisfactoriamente","user":user});
                                }else{
                                    res.send({"mensaje":"Ocurrio un error al registrar el usuario"});
                                } 
                            }
                    });
                }

            }
        });

    }catch(ex){
        res.status(500);
        res.send({"detalle":ex+""});
    }
};

exports.userById = function (req, res) {
    try{
        User.findById(req.params.id, function (err, user) {        
            if (err) {
                res.status(400);
                res.send({"mensaje":"Ocurrio al encontrar el usuario ","detalle":err.message});
            }else{
                res.status(200);
                res.send({"user":user});
            }
        })
    }catch(ex){
        res.status(500);
        res.send({"detalle":ex+""});
    }
};

exports.userUpdate = function (req, res) {
    try{
        User.findOneAndUpdate({_id:req.params.id}, {$set: req.body},{new: true}, function (err, user) {
            if (err) {
                res.status(400);
                res.send({"mensaje":"Ocurrio un error al actualizar el usuario ","detalle":err.message});
            }else{
                res.status(200);
                if(user!=null){
                    res.send({"mensaje":"Usuario actualizado satisfactoriamente","user":user});
                }else{
                    res.send({"mensaje":"No se encontro el usuario con el id indicado"});
                } 
            }
        });
    }catch(ex){
        res.status(500);
        res.send({"detalle":ex+""});
    }
};

exports.userDelete = function (req, res) {
    try{
        User.findOneAndRemove({_id:req.params.id}, function (err,user) {
            if (err) {
                res.status(400);
                res.send({"mensaje":"Ocurrio un error al eliminar el usuario ","detalle":err.message});
            }else{
                res.status(200);
                if(user!=null){
                    res.send({"mensaje":"Usuario Eliminado","user":user});
                }else{
                    res.send({"mensaje":"No se encontro un usuario con el id indicado"});
                } 
            }
        })
    }catch(ex){
        res.status(500);
        res.send({"detalle":ex+""});
    }
};

exports.userLogin = function (req, res) {
    try{
        User.findOneAndUpdate({_id:req.params.id}, {$set: req.body},{new: true}, function (err, user) {
            if (err) {
                res.status(400);
                res.send({"mensaje":"Ocurrió un error al loguearse el usuario","detalle":err.message});
            }else{
                res.status(200);
                if(user!=null){
                    res.send({"mensaje":"Login Correcto","user":user});
                }else{
                    res.send({"mensaje":"Login Incorrecto. Los datos son inválidos"});
                } 
            }
        });
    }catch(ex){
        res.status(500);
        res.send({"detalle":ex+""});
    }
};

exports.userLogin2 = function (req, res){
    try{
        User.findOne({email:req.body.email}, function (err, user) {        
            if (err) {
                res.status(400);
                res.send({"mensaje":"Ocurrió un error al loguearse el usuario","detalle":err.message});
            }else{
                if(user!=null){                
                    if (bcrypt.compareSync( req.body.password , user.password)) {  
                        
                        var tokenData = {
                                username: user.email
                              };
                            
                          var token = jwt.sign(tokenData,JWT_SECRET_PASS, {
                             expiresIn: 60 * 60 * 1 // expires in 1 hours
                          });
                        let login = '{"$set":{"logged":true,"token":"'+token+'"}}';
                        util_sisgein.printLog(login);
                        User.findOneAndUpdate({_id:user._id}, JSON.parse(login),{new: true}, function (err, user) {
                            if (err) {
                                res.status(400);
                                res.send({"mensaje":"Ocurrió un error al loguearse el usuario","detalle":err.message});
                            }else{
                                if(user!=null){
                                  
                                    res.send({"mensaje":"Login Correcto","user":user});
                                    res.status(200);
                                    
                                }else{
                                    res.status(400);
                                    res.send({"mensaje":"Login Incorrecto. Los datos son inválidos"});
                                } 
                            }
                        });
                        console.log('Password matched!'+user.password+'-'+req.body.password);  
                        console.log(user); 
                        //res.send({"mensaje":"Login Correcto","user":user});
                    } else {  
                        res.status(400);
                        console.log('Password doesn\'t match');  
                        res.send({"mensaje":"Login Incorrecto. Los datos son inválidos", "usuario":user});
                    }
                } else {  
                        res.status(400);
                        console.log('usuario no registrado');  
                        res.send({"mensaje":"usuario no registrado"});
                }
            }
        })
    }catch(ex){
        res.status(500);
        res.send({"detalle":ex+""}); 
    }
};

exports.userLogout = function (req, res){
    if (req.session) {
        // delete session object
        req.session.destroy(function (err) {
          if (err) {
              res.status(400);
              res.send({"mensaje":"no puedo desloguearse"});
              console.log('no puedo desloguearse');  
          } else {
              res.status(200);
              res.send({"mensaje":"usuario deslogueado"});
              console.log('se puedo desloguearse');  
          }
        });
      }
};

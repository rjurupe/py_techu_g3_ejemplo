var Producto = require('../models/producto_model');
var util_sisgein = require('../util/util_sisinv');

exports.getAll = function (req, res) {
    try{
        Producto.find({}).exec(function (err, productos) {
            if (err) {
                res.status(400);
                res.send({"mensaje":"Ocurrio un error al obtener los productos ","detalle":err.message});
            }else{
                res.status(200);
                res.send({"productos":productos});
            }            
        });
    }catch(ex){
        res.status(500);
        res.send({"detalle":ex+""});
    }
};

exports.productoCreate = function (req, res) {
    try{
        var  producto = new Producto(
            {
                name: req.body.name,
                price: req.body.price
            }
        );
        producto.save(function (err) {            
            if (err) {
                res.status(400);
                res.send({"mensaje":"Ocurrio un error al registrar el producto ","detalle":err.message});
            }else{
                res.status(200);
                if(producto!=null){
                    res.send({"mensaje":"Producto Agregado","producto":producto});
                }else{
                    res.send({"mensaje":"Ocurrio un error al registrar el producto"});
                } 
            }
        });
    }catch(ex){
        res.status(500);
        res.send({"detalle":ex+""});
    }
};
/*
exports.documentById = function (req, res) {
    try{
        Document.findById(req.params.id).exec(function (err, document) {        
            if (err) {
                res.status(400);
                res.send({"mensaje":"Ocurrio un error al encontrar el documento ","detalle":err.message});
            }else{
                res.status(200);
                res.send({"document":document});
            }
        })
    }catch(ex){
        res.status(500);
        res.send({"detalle":ex+""});
    }
};
*/
exports.productoUpdate = function (req, res) {
    try{
        Producto.findOneAndUpdate({_id:req.params.id}, {$set: req.body},{new: true,runValidators: true}, function (err, producto) {
            if (err) {
                res.status(400);
                res.send({"mensaje":"Ocurrio un error al actuializar el producto ","detalle":err.message});
            }else{
                res.status(200);
                if(producto!=null){
                    res.send({"mensaje":"producto Actualizado","producto":producto});
                }else{
                    res.send({"mensaje":"No se encontro un producto con el id indicado"});
                } 
            }
        });
    }catch(ex){
        res.status(500);
        res.send({"detalle":ex+""});
    }
};
/*
exports.documentDelete = function (req, res) {
    try{
        Document.findOneAndRemove({_id:req.params.id}, function (err,document) {
            if (err) {
                res.status(400);
                res.send({"mensaje":"Ocurrio un error al eliminar el documento ","detalle":err.message});
            }else{
                res.status(200);
                if(document!=null){
                    res.send({"mensaje":"documento Eliminado","document":document});
                }else{
                    res.send({"mensaje":"No se encontro un documento con el id indicado"});
                } 
            }
        })
    }catch(ex){
        res.status(500);
        res.send({"detalle":ex+""});
    }
};*/

var mongoose = require('mongoose');

const UserSchema = mongoose.Schema({
      first_name: {
        type: String, 
        required: [true, 'Debe ingresar el nombre']
      },
      last_name: {
        type: String,
        required: [true, 'Debe ingresar los apellidos']
      },
      email: {
        type: String,
        required: [true, 'Debe ingresar el email']
      },
      password: {
        type: String,
        required: [true, 'Debe ingresar contraseña']
      },
      rol: {
        type: String,
        enum: ['TAM', 'EYO','CAN']
      },
      logged: {
        type: Boolean
      },
      token: {
        type: String
      }
  });
   
  module.exports = mongoose.model('User', UserSchema,"user");
var express = require ('express');
var jwt = require('jsonwebtoken');
var util_sisgein = require('./util/util_sisinv');
var bodyParser = require('body-parser');
var productoRoute = require('./routes/producto_route');
var userRoute = require('./routes/user_route');


const URL_API_REST = process.env.URL_API_REST;
const DB_CONECTION_MONGOOSE = process.env.DB_CONECTION_MONGOOSE;
const JWT_SECRET_PASS = process.env.JWT_SECRET_PASS;
const URL_LOGIN = process.env.URL_LOGIN;
const DEFAULT_PORT = 3000;
const PORT = process.env.PORT || DEFAULT_PORT;
require('dotenv').config();

const cors = require('cors');

var app = express();
app.use(cors());

app.use(bodyParser.json({limit:'50mb'}));
app.use(bodyParser.urlencoded({limit:'50mb',extended: true}));


//*****Lineas agregadas para que el NAVEGADOR no bloquee las peticiones al MLAB (otro dominio)
//*****por defecto*****//
app.options('*', cors());

app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});


//Controllers
app.use(URL_API_REST+'/productos',productoRoute);
app.use(URL_API_REST+'/users',userRoute);

//moongoose connection

var mongoose = require('mongoose');
mongoose.connect(DB_CONECTION_MONGOOSE, { useNewUrlParser: true });
mongoose.Promise = global.Promise;
var db = mongoose.connection;
db.on('error', console.error.bind(console, 'MongoDB connection error:'));


app.listen(PORT,function(){
    util_sisgein.printLog("API SISINV escuchando en la URL: "+URL_API_REST+":"+PORT);
});
